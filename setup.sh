#!/bin/sh
# This script starts from your home directory, and then connects each file in
# the configs/ folder to its correct location where the system expects it.

# Prerequisites:
# configs folder is in home directory
# Installed programs:
# * vim
# * tmux
# * zsh
# * i3wm
# * rofi
# * git
# * alacritty

cd $HOME

# Folders
ln -sf ~/configs/.tmux ~/.tmux
ln -sf ~/configs/.zsh_conf ~/.zsh_conf

# Files
ln -sf ~/configs/.bashrc ~/.bashrc
ln -sf ~/configs/.bash_profile ~/.bash_profile
ln -sf ~/configs/.gdbinit ~/.gdbinit
ln -sf ~/configs/.gdb_history ~/.gdb_history
ln -sf ~/configs/.gitconfig ~/.gitconfig
ln -sf ~/configs/.i3config ~/.config/i3/config
ln -sf ~/configs/.i3status.conf ~/.i3status.conf
ln -sf ~/configs/i3status-rust-config.toml ~/.config/i3status-rust/config.toml
ln -sf ~/configs/.profile ~/.profile
ln -sf ~/configs/.tmux.conf ~/.tmux.conf
ln -sf ~/configs/.viminfo ~/.viminfo
ln -sf ~/configs/.vimrc ~/.vimrc
ln -sf ~/configs/.zprofile ~/.zprofile
ln -sf ~/configs/.zshrc ~/.zshrc
ln -sf ~/configs/alacritty.yml ~/.config/alacritty/alacritty.yml
ln -sf ~/configs/roficonfig ~/.config/rofi/config.rasi
