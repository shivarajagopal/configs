# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob correct PROMPT_SUBST
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/shiva/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export ZSH=/home/shiva/.antigen/bundles/robbyrussell/oh-my-zsh
export ZSH_CACHE_DIR=/home/shiva/.antigen/bundles/robbyrussell/oh-my-zsh/cache/

export ZSH_CONFIG_PATH=${HOME}/.zsh_conf
[ -f ~/.skim.zsh ] && source ~/.skim.zsh
source ${ZSH_CONFIG_PATH}/.zsh_antigen
source ${ZSH_CONFIG_PATH}/.zsh_bindkeys
source ${ZSH_CONFIG_PATH}/.zsh_ls
source ${ZSH_CONFIG_PATH}/.zsh_alias
source ${ZSH_CONFIG_PATH}/.zsh_misc
source ${ZSH_CONFIG_PATH}/.zsh_git
source ${ZSH_CONFIG_PATH}/.zsh_keybindings
source ${ZSH_CONFIG_PATH}/.zsh_prompt
source ${ZSH_CONFIG_PATH}/.zsh_gems

if ! { [ "$TERM" = "screen" ] && [ -n "$TMUX" ]; } then
  tmux
fi
export TERM=xterm-256color

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/shiva/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/shiva/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/shiva/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/shiva/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

