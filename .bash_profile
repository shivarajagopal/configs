echo "Running .bash_profile"
test -f ~/.profile && . ~/.profile
test -f ~/.bashrc && . ~/.bashrc

export PATH="$HOME/.cargo/bin:$PATH"
